NORMAL="\[\e[0m\]"
RED="\[\e[1;31m\]"
GREEN="\[\e[1;32m\]"

if [ "`id -u`" -eq 0 ]; then
  UF='# '
else
  UF='$ '
fi

export PS1="$GREEN[DOCKER \u@\h]$NORMAL:\w$UF "

