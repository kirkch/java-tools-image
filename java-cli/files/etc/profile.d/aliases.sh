alias gct="./gradlew -Dmosaics.junit.TimeoutSeconds=40 clean test"
alias gt="./gradlew -Dmosaics.junit.TimeoutSeconds=40 test"
alias gc="./gradlew clean"
alias gs="./gradlew status"
