
. ~/.bashrc
. ~/.git-completion.bash

# Run all shell scripts under /etc/profile.d
for f in /etc/profile.d/*.sh; do . $f; done
