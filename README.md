Docker Image:  Java Tools
=========================

This project creates a docker image that houses tools for working with Java.  It is an Ubuntu
image that includes common unix command line tools, docker, ansible and terraform.


Directory Layout
================

    /bin        -- commands to run to build, run and test the docker image
    /java-cli   -- create the command line version of the docker image for java devs
    /java-ui    -- add ui tools to the base java-cli docker image
 

Commands
========

    /bin/build-cli.sh <cliImageName=java-cli>
        tells docker to create the new images
    /bin/build-ui.sh <uiImageName=java-ui>
        tells docker to create the new images
    /bin/env.sh <imageNameToRun=java-tools>
        runs the container and maps the current working directory into the images work space 
    /bin/test-cli.sh
        runs java and gradle via the image to ensure that they have been installed correctly
    /bin/test-ui.sh
        runs java and gradle via the image to ensure that they have been installed correctly



DEV ENV with UI
===============

For apps such as intellij, firefox and datagrip that all have a ui.  Install
xlaunch (windows) or xquartz (osx) and set the DISPLAY environment variable 
to have the UI launch.

xlaunch:  https://sourceforge.net/projects/vcxsrv/files/vcxsrv/


OSX:  https://www.xquartz.org/ tested with 2.8.1

> run xquartz, and tick XQuartz->preferences->Security->Allow connections from clients
> restart xquartz
> xhost + 127.0.0.1
> docker run -ti -e DISPLAY=host.docker.internal:0 IMAGE




IDEAS
=====

- publishing docker images is the best way to pass them between ci stages, however the
  storage mounts up and we are yet to get a good way to clear out from gitlab
  currently we occasionally clear all images out.
