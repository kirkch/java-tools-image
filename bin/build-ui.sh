#!/bin/bash

set -eux

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null 2>&1 && pwd )"

CLI_IMAGE=${1:-java-cli}
UI_IMAGE=${2:-java-ui}
CACHE_IMAGE=registry.gitlab.com/kirkch/java-tools-image/master/java-ui:latest

docker pull $CACHE_IMAGE || true
docker build --tag ${UI_IMAGE} \
             --build-arg BUILDKIT_INLINE_CACHE=1 \
             --build-arg TOOLS_BASE_IMAGE=${CLI_IMAGE} \
             --cache-from $CACHE_IMAGE \
             $DIR/java-ui
