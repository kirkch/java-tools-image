#!/bin/bash

set -eux

for i in $(docker ps -a -q)
do
  docker rm $i
done


for i in $(docker images -aq)
do
  docker rmi -f $i
done

