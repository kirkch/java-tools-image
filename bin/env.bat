
set TOOLS_VER=latest
set IMAGE=registry.gitlab.com/mosaics1/java-tools-image/master/java-ui:%TOOLS_VER%
set PROJECT_DIR=%CD%
set HOME=%USERPROFILE%

for %%I in (.) do set PROJECT_NAME=%%~nxI

docker run -it --rm --privileged --name "env-%PROJECT_NAME%" --hostname "%PROJECT_NAME%" ^
           --workdir /projects ^
           -v "%PROJECT_DIR%:/projects" -v "%HOME%/.ssh:/root/.ssh" ^
           -v "%HOME%/.gitconfig:/root/.gitconfig" ^
           -v "%HOME%/.docker:/root/.docker" ^
           -v "%HOME%/.gradle-%PROJECT_NAME%:/root/.gradle" ^
           -v "%HOME%/.m2:/root/.m2" -v "%HOME%/.config:/root/.config" ^
           -e DISPLAY=host.docker.internal:0  ^
           %IMAGE% /bin/bash -l
