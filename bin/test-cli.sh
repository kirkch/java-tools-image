#!/bin/bash

set -exu

IMAGE=${1:-java-cli}

docker run --rm -t ${IMAGE} /bin/bash -l -c 'java --version'
docker run --rm -t ${IMAGE} /bin/bash -l -c 'gradle --version'
docker run --rm -t ${IMAGE} /bin/bash -l -c 'terraform --version'
docker run --rm -t ${IMAGE} /bin/bash -l -c 'ansible --version'
