#!/bin/bash

set -eux

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null 2>&1 && pwd )"

IMAGE=${1:-java-cli}
CACHE_IMAGE=registry.gitlab.com/kirkch/java-tools-image/master/java-cli:latest

docker pull $CACHE_IMAGE || true
docker build --tag ${IMAGE} \
             --build-arg BUILDKIT_INLINE_CACHE=1 \
             --cache-from $CACHE_IMAGE \
             $DIR/java-cli
