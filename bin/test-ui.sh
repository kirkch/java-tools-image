#!/bin/bash

set -exu

IMAGE=${1:-java-ui}

docker run --rm -t ${IMAGE} /bin/bash -l -c 'java --version'
docker run --rm -t ${IMAGE} /bin/bash -l -c 'which idea.sh'
docker run --rm -t ${IMAGE} /bin/bash -l -c 'which datagrip.sh'
docker run --rm -t ${IMAGE} /bin/bash -l -c 'which firefox'
