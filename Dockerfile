FROM ubuntu:20.04 AS dev-ui

#### INSTALL INTELLIJ

RUN apt-get update && \
    apt-get install -y git  curl
RUN curl -L -o /tmp/intellij.tar.gz https://download.jetbrains.com/idea/ideaIU-2021.2.3.tar.gz && \
    mkdir -p /opt/intellij && \
    cd /opt/intellij && \
    tar -xvf /tmp/intellij.tar.gz && \
    rm /tmp/intellij.tar.gz && \
    ln -s /opt/intellij/idea-IU-212.5457.46  /opt/intellij/current


#### INSTALL DATAGRIP

RUN curl -L -o /tmp/datagrip.tar.gz https://download.jetbrains.com/datagrip/datagrip-2021.2.4.tar.gz && \
    mkdir -p /opt/datagrip && \
    cd /opt/datagrip && \
    tar -xvf /tmp/datagrip.tar.gz && \
    rm /tmp/datagrip.tar.gz && \
    ln -s /opt/datagrip/DataGrip-2021.2.4 /opt/datagrip/current


FROM ubuntu:20.04

# NB installing larger ui files first to reduce the most frequent rebuild times
#    these files do not change as frequently as gradle and the jdk
#    and when they do they take a long time to update (due to size)

COPY --from=dev-ui /opt /opt


#### INSTALL COMMON COMMAND LINE TOOLS

# set timezone so that the following installs do not ask for it
# https://askubuntu.com/questions/909277/avoiding-user-interaction-with-tzdata-when-installing-certbot-in-a-docker-contai/1098881#1098881
ENV TZ=Europe/London
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# to check available versions:  apt-cache madison ansible   or    apt-cache policy ansible
RUN apt-get update && \
    apt-get install -y git  curl vim ssh wget unzip python3 python3-pip iputils-ping net-tools


# SUPPORT APPS WITH UIs (eg intellij)
RUN apt-get update && \
    apt-get install -y 'x11vnc=0.9.16-3' \
                       'xvfb' \
                       'firefox'


####  INSTALL JDK15
#     See https://jdk.java.net/17/

ARG JDK_VERSION=17
ARG JDK_DOWNLOAD_URL=https://download.java.net/java/GA/jdk17.0.1/2a2082e5a09d4267845be086888add4f/12/GPL/openjdk-17.0.1_linux-x64_bin.tar.gz
ARG JDK_DIR_NAME=jdk-17.0.1
ENV JAVA_HOME=/opt/jdk/current

RUN curl -L -o /root/jdk.tgz $JDK_DOWNLOAD_URL && \
    mkdir -p /opt/jdk && \
    cd /opt/jdk && \
    tar -xf /root/jdk.tgz && \
    rm /root/jdk.tgz && \
    ln -s /opt/jdk/$JDK_DIR_NAME /opt/jdk/current


####  INSTALL TERRAFORM

ARG TERRAFORM_VER="1.0.10"
RUN wget https://releases.hashicorp.com/terraform/${TERRAFORM_VER}/terraform_${TERRAFORM_VER}_linux_amd64.zip && \
    unzip terraform_${TERRAFORM_VER}_linux_amd64.zip && \
    mv terraform /usr/local/bin/ && \
    rm terraform_${TERRAFORM_VER}_linux_amd64.zip

#### INSTALL ANSIBLE

RUN apt-get install -y ansible=2.9.6+dfsg-1


#### INSTALL GRADLE

# NB remember to update gradlew on each of the projects:  ./gradlew wrapper --gradle-version 6.6.1

ARG GRADLE_VERSION=7.3-rc-3
ARG GRADLE_DOWNLOAD_URL=https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip
ARG GRADLE_DIR_NAME=gradle-${GRADLE_VERSION}

RUN curl -L -o /tmp/gradle.zip $GRADLE_DOWNLOAD_URL && \
    mkdir -p /opt/gradle && \
    cd /opt/gradle && \
    unzip /tmp/gradle.zip && \
    rm /tmp/gradle.zip && \
    ln -s /opt/gradle/$GRADLE_DIR_NAME /opt/gradle/current && \
    cd /tmp && \
    mkdir dummy-project && cd dummy-project && \
    /opt/gradle/current/bin/gradle init --type basic --dsl kotlin --incubating --project-name foo && \
    ./gradlew && \
    cd / && \
    rm -rf /tmp/dummy-project

#### PREP PYTHON

RUN pip3 install pandas

##################

COPY tools-cli/files/ /

#ENTRYPOINT ["/bin/bash", "-l"]
